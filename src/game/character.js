class Character {

    constructor(id, name, minDamage, maxDamage, avatar, life) {
        this.name = name;
        this.id = id;
        this.life = life;
        this.initialLife = life;
        this.minDamage = minDamage;
        this.maxDamage = maxDamage;
        this.avatar = avatar;
    }

    get isDead() {
        return this.life == 0;
    }

    takeDamage(damageToTake) {
        const newLife = this.life - damageToTake;
        this.life = newLife >= 0 ? newLife : 0;
    }

    restoreLife() {
        this.life = this.initialLife;
    }
}

module.exports = Character;