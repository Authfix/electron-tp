const fs = require("fs");
const path = require("path");
const Game = require("./game");
const Character = require("./character");
const { app } = require('electron')

const game = createGame();

/**
 * Create the game.
 * Extract game info from game.json file et associate to
 * the corresponding game class
 */
function createGame() {
    const gameBuffer = fs.readFileSync(path.join(app.getAppPath(), "src", "assets", "game.json"));
    const gameObject = JSON.parse(gameBuffer);
    
    const enemies = [];
    
    const player = new Character(gameObject.player.id, gameObject.player.name, gameObject.player.minDamage, gameObject.player.maxDamage, gameObject.player.avatar, gameObject.player.life);

    gameObject.enemies.forEach(enemy => {
        enemies.push(new Character(enemy.id, enemy.name, enemy.minDamage, enemy.maxDamage, enemy.avatar, enemy.life));
    });
    
    return new Game(player, enemies);
}

module.exports = {
  getGame: function() {
    return game;
  }
};
