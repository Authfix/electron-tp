class Game {
  constructor(player, enemies) {
    this.player = player;
    this.enemis = enemies;
  }

  getCombat(enemyId) {
    const enemyToFight = this.enemis.find(enemy => {
      return enemy.id == enemyId;
    });

    return {
      player: this.player,
      enemy: enemyToFight
    };
  }

  async makeFight(enemiToFightId, notifier) {
    const enemyToFight = this.enemis.find(enemi => {
      return enemi.id == enemiToFightId;
    });

    if (enemyToFight == undefined) {
      throw new Error("Character not found");
    }

    while (!enemyToFight.isDead && !this.player.isDead) {
      const playerDamages = this.randomIntFromInterval(
        this.player.minDamage,
        this.player.maxDamage
      );

      enemyToFight.takeDamage(playerDamages);
      notifier({
        character: enemyToFight,
        damages: playerDamages
      });

      if (enemyToFight.isDead) {
        break;
      }

      await this.sleep(500);

      const enemyDamages = this.randomIntFromInterval(
        enemyToFight.minDamage,
        enemyToFight.maxDamage
      );

      this.player.takeDamage(enemyDamages);
      notifier({
        character: this.player,
        isPlayer: true,
        damages: enemyDamages
      });

      await this.sleep(500);
    }

    const winner = enemyToFight.isDead ? this.player : enemyToFight;

    if (!this.player.isDead) {
      this.player.restoreLife();
    }

    return winner;
  }

  randomIntFromInterval(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }
}

module.exports = Game;
