const { app, BrowserWindow, ipcMain, Notification } = require("electron");
const helper = require("./helper");
const menu = require("./menu");
const GameManager = require("./game/game.manager");
const path = require("path");

let game = GameManager.getGame();

menu.setupMenu();

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require("electron-squirrel-startup")) {
  // eslint-disable-line global-require
  app.quit();
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

const createWindow = () => {
  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 800,
    height: 600,
    webPreferences: {
      preload: path.join(__dirname, "preload.js")
    }
  });

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index/index.html`);

  if (helper.isDevelopment()) {
    mainWindow.webContents.openDevTools();
  }

  // Emitted when the window is closed.
  mainWindow.on("closed", () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on("ready", createWindow);

// Quit when all windows are closed.
app.on("window-all-closed", () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

ipcMain.on("game-query", function(event, message) {
  event.sender.send("game-result", game);
});

ipcMain.on("combat-make", async function(event, message) {
  const winner = await game.makeFight(message.enemy.id, damages => {
    event.sender.send("combat-log-damages", damages);
  });

  event.sender.send("combat-finished");

  const notification = new Notification({
    title: "Combat terminé",
    body: `${winner.name} a gagné le combat`
  });

  notification.show();
});

ipcMain.on("combat-query", function(event, message) {
  const combat = game.getCombat(message.enemyId);
  event.sender.send("combat-result", combat);
});
