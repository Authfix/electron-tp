function getUrlParameters(query) {
  var vars = query.split("&");
  var query_string = {};
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split("=");
    var key = decodeURIComponent(pair[0]);
    var value = decodeURIComponent(pair[1]);
    // If first entry with this name
    if (typeof query_string[key] === "undefined") {
      query_string[key] = decodeURIComponent(value);
      // If second entry with this name
    } else if (typeof query_string[key] === "string") {
      var arr = [query_string[key], decodeURIComponent(value)];
      query_string[key] = arr;
      // If third or later entry with this name
    } else {
      query_string[key].push(decodeURIComponent(value));
    }
  }
  return query_string;
}

function setupCharacter(elementId, character, right) {
  const context = {
    ...character,
    right: right,
    left: !right,
    isDead: character.life == 0
  };

  $.get(
    "../templates/character.hbs",
    function(data) {
      var template = Handlebars.compile(data);
      $(elementId).html(template(context));
    },
    "html"
  );
}

window.bridge.handleDamageLogs = function(damages) {
  const combatLogsContainer = document.getElementById("combat-logs");

  const combatLogs = document.createElement("li");
  combatLogs.className = "list-group-item";
  combatLogs.textContent = `${damages.character.name} a pris ${
    damages.damages
  } dommages`;

  if (damages.isPlayer) {
    window.combat.player = damages.character;
    setupCharacter("#player", damages.character);
  } else {
    window.combat.enemy = damages.character;
    setupCharacter("#enemy", damages.character, true);
  }

  combatLogsContainer.appendChild(combatLogs);
};

window.bridge.setupCombat = function(combat) {
  window.combat = combat;
  setupCharacter("#player", combat.player);
  setupCharacter("#enemy", combat.enemy, true);
};

window.bridge.combatCompleted = function() {
  const combatButton = document.getElementById("combat-button");
  combatButton.textContent = "Retourner à l'accueil";
  combatButton.disabled = false;
  combatButton.className = "offset-4 col-4 btn-lg btn-primary";
};

const urlQuery = window.location.search.substring(1);
const urlParameters = getUrlParameters(urlQuery);

const combatButton = document.getElementById("combat-button");
combatButton.addEventListener("click", function() {
  if (window.combat.player.life == 0 || window.combat.enemy.life == 0) {
    window.location = "../index/index.html";
  } else {
    window.bridge.makeCombat(window.combat);
    combatButton.disabled = true;
  }
});

window.bridge.getCombat(urlParameters.id);
