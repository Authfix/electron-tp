const { ipcRenderer } = require("electron");

ipcRenderer.on("game-result", function(event, message) {
  window.bridge.setGameInfo(message);
});

ipcRenderer.on("combat-result", function(event, message) {
  window.bridge.setupCombat(message);
});

ipcRenderer.on("combat-log-damages", function(event, message){
  window.bridge.handleDamageLogs(message);
});

ipcRenderer.on("combat-finished", function(){
  window.bridge.combatCompleted();
})

window.bridge = {
  getGame: getGame,
  getCombat: getCombat,
  makeCombat: makeCombat
};

function makeCombat(combat) {
  ipcRenderer.send("combat-make", combat);
}

function getGame() {
  ipcRenderer.send("game-query");
}

function getCombat(enemyId) {
  ipcRenderer.send("combat-query", {
    enemyId: enemyId
  });
}
