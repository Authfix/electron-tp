window.bridge.setGameInfo = setGameInfo;
window.bridge.getGame();

function setGameInfo(game) {
  setPlayer(game.player);
  setEnemies(game);
}

function setPlayer(userToSet) {
  const context = {
    ...userToSet,
    left: true,
    isDead: userToSet.life == 0
  }

  $.get("../templates/character.hbs", function(data) {
    var template = Handlebars.compile(data);
    $("#user-card").html(template(context));
  }, "html");
}

function setEnemies(game) {
  $.get("../templates/enemies.hbs", function(data) {
    var template = Handlebars.compile(data);
    $("#enemy-list").html(template(game));
  }, "html");
}
