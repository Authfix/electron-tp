const { app } = require("electron");

module.exports = {
  /**
   * Define if the application is running under development mode.
   * We don't use NODE_ENV (as a node application) because NODE_ENV is not set by
   * default.
   */
  isDevelopment: function() {
    return !app.isPackaged;
  },

  /**
   * Get value indicating if we are on a Mac or not
   */
  isMac: function() {
    return process.platform === "darwin";
  }
};
