const helper = require("./helper");
const { app, Menu } = require("electron");

exports.setupMenu = function() {
  const template = [
    ...(helper.isMac()
      ? [
          {
            label: app.getName(),
            submenu: [
              { role: "about" },
              { type: "separator" },
              { role: "services" },
              { type: "separator" },
              { role: "hide" },
              { role: "hideothers" },
              { role: "unhide" },
              { type: "separator" },
              { role: "quit" }
            ]
          }
        ]
      : []),
    {
      label: "File",
      submenu: [helper.isMac() ? { role: "close" } : { role: "quit" }]
    },
    ...(helper.isMac()
      ? [
          {
            label: "Edit",
            submenu: [
              { role: "pasteAndMatchStyle" },
              { role: "delete" },
              { role: "selectAll" },
              { type: "separator" },
              {
                label: "Speech",
                submenu: [{ role: "startspeaking" }, { role: "stopspeaking" }]
              }
            ]
          }
        ]
      : []),
    ...(helper.isDevelopment()
      ? [
          {
            label: "View",
            submenu: [
              { role: "reload" },
              { role: "forcereload" },
              { role: "toggledevtools" },
              { type: "separator" },
              { role: "resetzoom" },
              { role: "zoomin" },
              { role: "zoomout" },
              { type: "separator" },
              { role: "togglefullscreen" }
            ]
          }
        ]
      : []),
    ...(helper.isMac()
      ? [
          {
            label: "Window",
            submenu: [
              { type: "separator" },
              { role: "front" },
              { type: "separator" },
              { role: "window" }
            ]
          }
        ]
      : []),
    {
      role: "help",
      submenu: [
        {
          label: "Learn More",
          click() {
            require("electron").shell.openExternalSync(
              "https://electronjs.org"
            );
          }
        }
      ]
    }
  ];

  const menu = Menu.buildFromTemplate(template);
  Menu.setApplicationMenu(menu);
};
